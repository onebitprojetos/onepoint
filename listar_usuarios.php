<!DOCTYPE html>
<html>
<head>	
	<meta charset="utf-8">
	<title>Usuários</title>
			
	<!-- css bootstrap -->
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

	<script src="https://kit.fontawesome.com/4c6b8b50cf.js"></script>
</head>
<body>
	<div class="container" style="width: 80%; margin-top: 50px;">
		<div style="text-align: right;">					
			<a class="btn btn-primary btn-sm" href="inicio.php" role="button" ><- Voltar</a>
		</div>
		<h4>Listagem de Membros Cadastrados</h4>
		<br>
	<table class="table">
	  <thead>
	    <tr>

	      <th scope="col">Código</th>
	      <th scope="col">Nome</th>
	      <th scope="col">CPF</th>
	      <th scope="col">Email</th>
	      <th scope="col">Tipo</th>
	      <th scope="col">Ação</th>
	      
	    </tr>
	  </thead>
	  <tbody>
	    
	      	<?php  
	      		include 'conexao.php';

	      		$sql = "SELECT id, nome, cpf, email, tipo FROM `usuario`";
	      		$busca = mysqli_query($conexao, $sql);

	      		while ($array = mysqli_fetch_array($busca)) {

	      			$id = $array['id'];
	      			$nome = $array['nome'];
	      			$cpf = $array['cpf'];
	      			$email = $array['email'];
	      			$tipo = $array['tipo'];
	      		
	      	?>
	      	<tr>
	      		<td><?php echo $id ?>           </td>
	      		<td><?php echo $nome ?>      </td>
	      		<td><?php echo $cpf ?>        </td>
	      		<td><?php echo $email ?> </td>
	      		<td><?php echo $tipo ?>   </td>
	      		<td><a class="btn btn-warning btn-sm" href="editar_usuario.php?id=<?php echo $id ?>" role="button"><i class="far fa-edit"></i>&nbsp;Editar</a>

	      		<a class="btn btn-danger btn-sm" href="excluir_usuario.php?id=<?php echo $id ?>" role="button"><i class="far fa-trash-alt"></i>&nbsp;Excluir</a></td>

	      		<?php } ?> <!-- fexa o while -->

	    </tr>
	  </tbody>
	</table>
	</div>





<!-- JavaScript bootstrap -->
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
</body>
</html>