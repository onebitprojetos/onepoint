<?php 

session_start();

if (isset($_SESSION['usuarioId'])) {

$nivelAcesso = $_SESSION['usuarioTipo'];

?>

<!DOCTYPE html>

<html>

<head>

<title>Menu</title>
<meta charset="utf-8">
<meta name="viewport" content="initial-scale=1.0, user-scalable=no">
<style type="text/css">
.linha{
	width: 500px; margin: auto;
}
.conteudo{
	width: 500px;
}
</style>
<!-- css bootstrap -->

<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

</head>

<body>
<?php if ($nivelAcesso==3 || $nivelAcesso==2 || $nivelAcesso==1): ?>

<br>

<?php if ($nivelAcesso==3): ?>
  <br>
  <br>
  
<div class="classe1" style="width: 100%; margin: auto;">

<div class="card" style="width: 300px; margin: auto;">

  <div class="card-body" style="width: 300px;">

    <h5 class="card-title">Adicionar Ponto</h5>

    <p class="card-text">Criar um novo registro para um membro. </p>

    <a href="adicionar_ponto.php" class="btn btn-primary">Adicionar</a>

  </div>

</div>
</div>
<?php endif ?>


<br>
<?php if ($nivelAcesso==3): ?>
<div class="classe1" style="width: 100%; margin: auto;">

<div class="card" style="width: 300px; margin: auto;">

  <div class="card-body" style="width: 300px;">

    <h5 class="card-title">Adicionar Usuário</h5>

    <p class="card-text">Adicionar um novo membro da sua empresa. </p>

    <a href="adicionar_usuario.php" class="btn btn-primary">Adicionar</a>

  </div>

</div>
</div>

<br>
<div class="classe1" style="width: 100%; margin: auto;">

<div class="card" style="width: 300px; margin: auto;">

  <div class="card-body" style="width: 300px;">

    <h5 class="card-title">Listar Usuários</h5>

    <p class="card-text">Listar todos os membros cadastrados na sua empresa. </p>

    <a href="listar_usuarios.php" class="btn btn-primary">Listar</a>

  </div>

</div>
</div>
<?php endif ?>
<br>
<div class="classe1" style="width: 100%; margin: auto;">

<div class="card" style="width: 300px; margin: auto;">

  <div class="card-body" style="width: 300px;">

    <h5 class="card-title">Bater meu Ponto</h5>

    <p class="card-text">Registrar seu ponto. </p>

    <a href="inserir_ponto.php" class="btn btn-primary">Registrar</a>

  </div>

</div>
</div>

<br>

<div class="classe1" style="width: 100%; margin: auto;">

<div class="card" style="width: 300px; margin: auto;">

  <div class="card-body" style="width: 300px;">

    <h5 class="card-title">Listar Pontos</h5>

    <p class="card-text">Ver todos os pontos registrados. </p>

    <a href="listar_pontos.php" class="btn btn-primary">Ver -></a>

  </div>

</div>
</div>
<br>
<div class="classe1" style="width: 100%; margin: auto;">

<div class="card" style="width: 300px; margin: auto;">

  <div class="card-body" style="width: 300px;">

    <h5 class="card-title">Sair</h5>

    <p class="card-text">Deslogar do sistema. </p>

    <a href="sair.php" class="btn btn-primary">Sair</a>

  </div>

</div>
</div>


<?php endif ?>


<!-- JavaScript bootstrap -->

<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>

<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

</body>

</html>

<?php 

}

else{

$_SESSION['erroLogin'] = "Usuário ou senha inválidos";

header("Location: index.php");

}

?>

