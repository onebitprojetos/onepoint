<?php 
session_start();

unset($_SESSION['usuarioId']);
unset($_SESSION['usuarioTipo']);
unset($_SESSION['usuarioCpf']);
unset($_SESSION['usuarioEmail']);
unset($_SESSION['usuarioCodEmpresa']);
unset($_SESSION['erroLogin']);

header("Location: index.php");
 ?>