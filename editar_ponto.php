<?php 
$id = $_GET['id'];

 ?>
 <!DOCTYPE html>
 <html>
 <head>
 	<meta charset="utf-8">
 	<title>Pontos Registrados </title>
 	<!-- css bootstrap -->
 	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
 </head>
 <body>
 	<div class="container" style="margin-top: 50px; width: 50%">
 		
 		<h4>Editar registro</h4>
 		<br>
 	<form action="_atualizar_registro.php" method="post">

 		<?php 
			include 'conexao.php';

	  		$sql = "SELECT * FROM `horario` WHERE id = $id;";
	  		$busca = mysqli_query($conexao, $sql);

	  		while ($array = mysqli_fetch_array($busca)) {

  			$id = $array['id'];
  			$usuario = $array['usuario'];
  			$datah = $array['datah'];
  			$hora_entrada = $array['hora_entrada'];
  			$hora_saida = $array['hora_saida'];

 		 ?>

 		<div class="form-group">
 			<label>Usuário</label>
 		    <input type="text" class="form-control" name="usuario" value="<?php echo $usuario ?>" disabled>	
        <input type="number" class="form-control" name="id" value="<?php echo $id ?>" style ="display: none;">    
 		</div>
 		<div class="form-group">
 			<label>Data</label>
 		    <input type="date" class="form-control" name="data" value="<?php echo $datah ?>">	    
 		</div>
 		<div class="form-group">
 			<label>Hora de Entrada</label>
 		    <input type="time" class="form-control" name="horaentrada" value="<?php echo $hora_entrada ?>">	  
 		</div>
 		<div class="form-group">
 			<label>Hora de Saída</label>
 		    <input type="time" class="form-control" name="horasaida" value="<?php echo $hora_saida ?>">	    
 		</div>
 		<div style="text-align: right;">
 			<button type="submit" class="btn btn-success" >Atualizar</button>
 		</div>
 		<?php } ?>
 	</form>
 	
 	</div>

 <!-- JavaScript bootstrap -->
 <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
 <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
 <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
 </body>
 </html>