<?php 
session_start();

 ?>

<!DOCTYPE html>
<html>
<head>  
  <meta charset="utf-8">
  <!-- para não diminuir meu box de login -->
  <meta name="viewport" content="initial-scale=1.0, user-scalable=no">
  <title>Login</title>
      
  <!-- css bootstrap -->
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

  <script src="https://kit.fontawesome.com/4c6b8b50cf.js"></script>
  <style type="text/css">
    .login{
      /*https://www.cssmatic.com/box-shadow */
      -webkit-box-shadow: 5px 6px 24px 3px rgba(163,157,163,0.27);
      -moz-box-shadow: 5px 6px 24px 3px rgba(163,157,163,0.27);
      box-shadow: 5px 6px 24px 3px rgba(163,157,163,0.27);
    }
  </style>
</head>
<body>


<div class="container login" style="width: 350px; margin-top: 10%; border-radius: 10px; border: 2px solid #f3f3f3">
  <div style="padding: 15px">
  <form action="_validar_login.php" method="post">

    <div class="form-group">
      <label >Email</label>
      <input type="email" class="form-control" name="email" placeholder="Digite seu email">
    </div>
    <div class="form-group">
      <label>Password</label>
      <input type="password" class="form-control" name="senha" placeholder="Digite sua senha">
    </div>
    <button type="submit" class="btn btn-primary">Entrar</button>

  </form>
  <div>
    <?php 
      if (isset($_SESSION['erroLogin'])) {
        echo $_SESSION['erroLogin'];
        unset($_SESSION['erroLogin']);
      }
     ?>
  </div>
</div>
</div>
<!-- JavaScript bootstrap -->
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
</body>
</html>