<?php 
$id = $_GET['id'];

 ?>
 <!DOCTYPE html>
 <html>
 <head>
 	<meta charset="utf-8">
 	<title>Editar Usuário </title>
 	<!-- css bootstrap -->
 	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
 </head>
 <body>
 	<div class="container" style="margin-top: 50px; width: 50%">
 		
 		<h4>Editar registro</h4>
 		<br>
 	<form action="_atualizar_usuario.php" method="post">

 		<?php 
			include 'conexao.php';

	  		$sql = "SELECT * FROM `usuario` WHERE id = $id;";
	  		$busca = mysqli_query($conexao, $sql);

	  		while ($array = mysqli_fetch_array($busca)) {

  			$id = $array['id'];
        $nome = $array['nome'];
        $cpf = $array['cpf'];
        $email = $array['email'];
        $tipo = $array['tipo'];

        echo $id;
        echo $nome ;
        echo $cpf ;
        echo $email ;
        echo $tipo ;
 		 ?>

 		<div class="form-group">
 			<label>Código</label>
      <input type="number" class="form-control" name="id" value="<?php echo $id ?>"  style="display: none;"> 
 		</div>
    <div class="form-group">
      <label>Nome</label>
      <input type="text" class="form-control" name="nome" value="<?php echo $nome ?>">           
    </div>
 		<div class="form-group">
 			<label>cpf</label>
 		    <input type="number" class="form-control" name="cpf" value="<?php echo $cpf ?>">	    
 		</div>
 		<div class="form-group">
 			<label>email</label>
 		    <input type="email" class="form-control" name="email" value="<?php echo $email ?>">	  
 		</div>
 		<div class="form-group">
      <label>Tipo de usuário</label>
      <select class="form-control form-control-sm" name="tipousuario">
        <option value="2">Comun</option>
        <option value="1">Administrador</option>
      </select>     
    </div>
 		<div style="text-align: right;">
 			<button type="submit" class="btn btn-success" >Atualizar</button>
 		</div>

 		<?php } ?>
 	</form>
 	
 	</div>

 <!-- JavaScript bootstrap -->
 <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
 <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
 <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
 </body>
 </html>