<?php 
session_start();
if (isset($_SESSION['usuarioId'])) {

	?>

		<!DOCTYPE html>
		<html>
		<head>
			<meta charset="utf-8">
			<title>Pontos Registrados </title>
			<!-- css bootstrap -->
			<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
		</head>
		<body>
			<div class="container" style="margin-top: 50px; width: 50%">
				<div style="text-align: right;">					
					<a class="btn btn-primary btn-sm" href="inicio.php" role="button" ><- Voltar</a>
				</div>
				
				<br>
				<h4>Registros dos pontos</h4>
				<br>

			<form action="_inserir_registro.php" method="post">
				<div class="form-group">
					<label>Código do Usuário</label>
				    <input type="text" class="form-control" name="usuario" placeholder="Digite o ID do usuário">	    
				</div>
				<div class="form-group">
					<label>Data</label>
				    <input type="date" class="form-control" name="data" placeholder="Data do registro">	    
				</div>
				<div class="form-group">
					<label>Hora de Entrada</label>
				    <input type="time" class="form-control" name="horaentrada" placeholder="Hora de chegada">	    
				</div>
				<div class="form-group">
					<label>Hora de Saída</label>
				    <input type="time" class="form-control" name="horasaida" placeholder="Hora de saída">	    
				</div>
				<div style="text-align: right;">
					<button type="submit" class="btn btn-success" >Registrar</button>
				</div>
				
			</form>

			</div>

		<!-- JavaScript bootstrap -->
		<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
		<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
		</body>
		</html>

		<?php }else{
$_SESSION['erroLogin'] = "Usuário ou senha inválidos";
	header("Location: index.php");
}